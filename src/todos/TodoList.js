import React from "react";
import TodoItem from "./TodoItem";
import NewTodo from "./NewTodo";
const TodoList = ({ todos = [{ text: 'Hello' }] }) => (
    <div className="list-wrapper">
        <NewTodo />
        {todos.map(todo => { <TodoItem todo={todo} /> })}
    </div>
)

export default TodoList;