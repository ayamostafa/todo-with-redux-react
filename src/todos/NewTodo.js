import React, { useState } from "react";
import './NewTodo.css';
const NewTodo = () => {
    const [input, setInput] = useState('');
    return (<div className="new-todo-form ">
        <input type="text" className="new-todo-input"
            value={input}
            placeholder="type new todo here..."
            onChange={e => setInput(e.target.value)}
        />
        <button className="new-todo-button">Create Todo</button>
    </div>);
};
export default NewTodo;